jQuery(document).ready(function(){

	function comentar(){
	
		setTimeout(function(){
		
			//	Usuario
			jQuery("#edit-field-comentar-anonimamente-und-none").click(function(){
				var text = jQuery("#edit-field-comentar-anonimamente-und").html();
				text = text.replace("N/D","Usuario");
				jQuery("#edit-field-comentar-anonimamente-und").html(text);
				jQuery("#edit-field-comentar-anonimamente-und-none").attr('checked', true);
				var usuario = jQuery(".nombre_c").html();
				usuario = usuario.replace('<span class="glyphicon glyphicon-user"></span>','');
				usuario = jQuery.trim(usuario);
				jQuery("#edit-field-nombres-und-0-value").val(usuario);
				jQuery("#edit-field-nombres-und-0-value").attr("disabled",false);
				comentar();
			});
			
			//	Anonimo
			jQuery("#edit-field-comentar-anonimamente-und-comentar-anonimamente").click(function(){
				jQuery("#edit-field-nombres-und-0-value").val("anonimo");
				jQuery("#edit-field-nombres-und-0-value").attr("disabled",true);
				comentar();
			});
			
		},1000);
		
	}


	jQuery("#edit-field-comentar-anonimamente-und-none").removeAttr("onclick");
	jQuery("#edit-field-comentar-anonimamente-und-comentar-anonimamente").removeAttr("onclick");
	comentar();
	setTimeout(function(){
		jQuery("#edit-field-comentar-anonimamente-und-none").click();
	},1000);

        //  Colocar nombre de usuario en popup para comentar, por defecto activar comentar con usuario y desbloquear rol    
    	jQuery(".init-modal-forms-comment-processed").click(function(){
	
		setTimeout(function(){
	
			var userName = "";

			userName = jQuery(".nombre_c").html();
			userName = userName.replace('<span class="glyphicon glyphicon-user"></span>','');
			userName = userName.trim();

			jQuery("#edit-field-nombres-und-0-value").val(userName);
		
                       jQuery("#edit-field-comentar-anonimamente-und-none").prop("checked",true);
                       jQuery("#edit-field-rol-und").removeProp("disabled");

		},2000);
	
	});

});