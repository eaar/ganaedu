

var birthday = jQuery(".field-name-field-fecha-nacimiento").find(".date-display-single").attr("content");
var d = new Date();
if(birthday){
  var edad = d.getFullYear() - birthday.substr(0,4) + " años";
  jQuery(".field-name-field-edad").find(".even").html(edad);
}


var ingreso = jQuery(".field-name-field-fecha-ingreso").find(".date-display-single").attr("content");
var retiro = jQuery(".field-name-field-fecha-retiro").find(".date-display-single").attr("content");
var d = new Date();

if(ingreso){
	ingreso = ingreso.substr(0,10);
}else{
  ingreso = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay();
}

if(retiro){
  retiro = retiro.substr(0,10);
}else{
  retiro = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay();
}

ingreso = new Date(ingreso).getTime();
retiro = new Date(retiro).getTime();
var dias = retiro - ingreso;
dias = dias/(1000*60*60*24);
var years = parseInt(dias/364);
var monts = parseInt(dias/30) - (years*12);
var mensaje = "";
if(years > 0)
  mensaje = years + "años";
if(monts > 0)
  mensaje = mensaje + monts + " meses";
jQuery(".field-name-field-tiempo-servicio").find(".even").html(mensaje);

