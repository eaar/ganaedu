var codes = '';

jQuery("#compare-content-wrapper").hide();
jQuery("#compare-view-mode-box").hide();

jQuery.each(jQuery(".field-name-field-codigo-dane").find(".field-item"), function(idx, item){
	codes += jQuery(item).text().trim() + ', ';
});

jQuery.each(jQuery("input[id*=edit-field-codigo-dane-value]"), function(idx, item){
	jQuery(item).val(codes);
});

jQuery("#edit-field-codigo-dane-value--5").val(codes);

jQuery(document).ajaxComplete(function() {
	jQuery("#compare-view-mode-box").hide();
    jQuery("#edit-combine").val("");
	jQuery("#edit-combine").attr("placeholder","Escribe Aquí");
	jQuery("#edit-combine--2").val("");
	jQuery("#edit-combine--2").attr("placeholder","Escribe Aquí");
	jQuery("#views-exposed-form-lista-escuelas-compare-block-1").find(".views-exposed-form").hide();
	jQuery("#views-exposed-form-resultados-prueba-saber-11-compara-block").find(".views-exposed-form").hide();
	jQuery("#views-exposed-form-compara-resultados-isce-block").find(".views-exposed-form").hide();
	jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").find(".views-exposed-form").hide();
	jQuery("#views-exposed-form-compara-gr-fica-isce-block").find(".views-exposed-form").hide();

	jQuery("#selIndicador").change(function(){
		jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").hide();
		jQuery("#isce_grafica").show();
		jQuery("#selIndicadorISCE").val(1);
	});
	
	jQuery("#selIndicadorISCE").change(function(){
		jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").show();
		jQuery("#isce_grafica").hide();
		jQuery("#selIndicador").val(0);
	});
});

jQuery(document).ready(function() {

	jQuery("#edit-combine").val("");
	jQuery("#edit-combine").attr("placeholder","Escribe Aquí");

	jQuery("#edit-submit-lista-escuelas-compare").click();

	jQuery("#edit-submit-resultados-prueba-saber-11-compara").click();
	
	jQuery("#edit-submit-compara-resultados-isce").click();
	
	jQuery("#edit-submit-compara-gr-fica-saber-11").click();
	
	jQuery("#edit-submit-resultados-saber-11-grafico-compara").click();
	
	jQuery("#edit-submit-compara-gr-fica-isce").click();

	jQuery("#block-views-ae108f04c245f186fb0cc746d4ed8271").hide();
	jQuery("#block-views-compara-resultados-isce-block").hide();
	jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").hide();
	jQuery("#isce_grafica").hide();

	jQuery("#general").attr("href", "javascript:void(0)");
	jQuery("#general").addClass('activoCompara')
	jQuery("#general").attr("onclick", "mostrar(0)");

	jQuery("#saber").attr("href", "javascript:void(0)");
	jQuery("#saber").attr("onclick", "mostrar(1)");

	jQuery("#isce").attr("href", "javascript:void(0)");
	jQuery("#isce").attr("onclick", "mostrar(2)");
	
	jQuery("#grafica_link").attr("href", "javascript:void(0)");
	jQuery("#grafica_link").attr("onclick", "mostrar(3)");
	
});

function mostrar(id){
	jQuery("#block-lista-escuela-compare").hide();
	jQuery("#block-views-ae108f04c245f186fb0cc746d4ed8271").hide();
	jQuery("#block-views-compara-resultados-isce-block").hide();
	jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").hide();
	jQuery("#isce_grafica").hide();
	
	if(id == 0){
		jQuery("#block-lista-escuela-compare").show();
		jQuery("#general").addClass('activoCompara');
		jQuery("#saber").removeClass('activoCompara');
		jQuery("#isce").removeClass('activoCompara');
		jQuery("#grafica_link").removeClass('activoCompara');
	}
	if(id == 1){
		jQuery("#block-views-ae108f04c245f186fb0cc746d4ed8271").show();
		jQuery("#block-menu-menu-menu-comparar").find(".first").removeClass("first");
		jQuery("#general").removeClass('activoCompara');
		jQuery("#saber").addClass('activoCompara');
		jQuery("#isce").removeClass('activoCompara');
		jQuery("#grafica_link").removeClass('activoCompara');
	}
	if(id == 2){
		jQuery("#block-views-compara-resultados-isce-block").show();
		jQuery("#block-menu-menu-menu-comparar").find(".first").removeClass("first");
		jQuery("#general").removeClass('activoCompara');
		jQuery("#saber").removeClass('activoCompara');
		jQuery("#isce").addClass('activoCompara');
		jQuery("#grafica_link").removeClass('activoCompara');
	}
	if(id == 3){
		jQuery("#selIndicador").val(0);
		jQuery("#block-views-508bb26c9d2381963c56ed99484dc948").show();
		jQuery("#isce_grafica").hide();
		jQuery("#block-menu-menu-menu-comparar").find(".first").removeClass("first");
		jQuery("#general").removeClass('activoCompara');
		jQuery("#saber").removeClass('activoCompara');
		jQuery("#isce").removeClass('activoCompara');
		jQuery("#grafica_link").addClass('activoCompara');
	}
}
