var uri = decodeURI(jQuery(location).attr("href"));
var data = uri.split("/");
var code = '';
if(data[5] == undefined){
  code = data[4];
}
else{
  code = data[5];
  var nombre = data[6];

  jQuery("#tituloEscuela").html("Código DANE: " + code + " Escuela:  " + nombre);
}

jQuery("#views-exposed-form-resultados-isce-block").find(".views-exposed-form").hide(); 
jQuery("#edit-field-codigo-dane-value").val(code);

jQuery(document).ready(function() {
    jQuery("#edit-submit-resultados-isce").click();
});

jQuery(document).ajaxComplete(function() {
	jQuery("#views-exposed-form-resultados-isce-block").find(".views-exposed-form").hide(); 
});
