<?php

/**
 * Implements hook_permission
 */
function forum_cloakpost_permission() {
  return array(
    'view cloaked usernames' => array(
      'title' => 'View cloaked usernames',
    )
  );
}


/**
 * Implements hook_form_alter().
 */
function forum_cloakpost_form_alter(&$form, $form_state, $form_id) {
  if ($form_id === 'comment_node_forum_form'
      || $form_id === 'forum_node_form') {
    $form['anonuser'] = array(
      '#type' => 'checkbox',
      '#title' => t('Post anonymously.'),
      '#description' => t('You may post this comment anonymously so that other forum users will not see this post linked to your user account. Once posted, you will not be able to edit your comment. Note: Moderators and Administrators can retrieve the original author information if necessary to prevent abuse.'),
      '#weight' => 30,
    );
  }
}


/**
 * Implements hook_comment_insert().
 */
function forum_cloakpost_comment_insert($comment) {
  if ($comment->anonuser === 1) {
    _forum_cloakpost_make_entity_anonymous('comment', $comment->cid, $comment->uid);
    db_update('node_comment_statistics')
      ->fields(array(
        'last_comment_uid' => 0,
      ))
      ->condition('nid', $comment->nid)
      ->condition('cid', $comment->cid)
      ->execute();
  }
}


/**
 * Implements hook_node_insert().
 */
function forum_cloakpost_node_insert($node) {
  if ($node->anonuser === 1) {
    _forum_cloakpost_make_entity_anonymous('node', $node->nid, $node->uid);
    db_update('node_comment_statistics')
      ->fields(array(
        'last_comment_uid' => 0,
      ))
      ->condition('nid', $node->nid)
      ->condition('cid', 0)
      ->execute();
  }
}


/**
 * Implements hook_comment_update().
 *
 * Comment has been updated to make it anonymous.
 */
function forum_cloakpost_comment_update($comment) {
  if ($comment->anonuser === 1 && $comment->uid) {
    _forum_cloakpost_make_entity_anonymous('comment', $comment->cid, $comment->uid);
  }
}


/**
 * Implements hook_node_update
 *
 * Node has been updated to make it anonymous.
 */
function forum_cloakpost_node_update($node) {
  if ($node->anonuser === 1 && $node->uid) {
    _forum_cloakpost_make_entity_anonymous('node', $node->nid, $node->uid);
    db_update('node_revision')
      ->fields(array('uid' => 0))
      ->condition('nid', $node->nid)
      ->execute();
  }
}


/**
 * Implements hook_node_view_alter
 */
function forum_cloakpost_node_view_alter(&$build) {
  $node = $build['#node'];

  // Check if forum node
  if ($node->type !== 'forum') {
    return;
  }

  // Check if node author is anonymous
  if ((int) $node->uid !== 0) {
    return;
  }

  // Check if user is a moderator of the forum, or has the permission
  $tid = _forum_access_get_tid($node);
  if (!forum_access_access('update', $tid) && !user_access('view cloaked usernames')) {
    return;
  }

  // Get username of original poster
  $entity_id = $build['taxonomy_forums']['#object']->nid;
  $uid = _forum_cloakpost_get_original_poster('node', $entity_id);
  if (!$uid) {
    return;
  }

  _forum_cloakpost_add_original_poster_markup($build, $uid);
}


/**
 * Implements hook_comment_view_alter
 */
function forum_cloakpost_comment_view_alter(&$build) {
  $comment = $build['#comment'];
  if ((int) $comment->uid !== 0) {
    return;
  }

  // Check if forum comment
  $node = node_load($comment->nid);
  if ($node->type !== 'forum') {
    return;
  }

  // Check if user should have access to see original posters
  $tid = _forum_access_get_tid($node);
  if (!forum_access_access('update', $tid) && !user_access('view cloaked usernames')) {
    return;
  }

  // Find the original poster
  $uid = _forum_cloakpost_get_original_poster('comment', $comment->cid);
  if (!$uid) {
    return;
  }

  _forum_cloakpost_add_original_poster_markup($build, $uid);
}


/**
 * Retrieve the original poster information from a blanked post.
 */
function _forum_cloakpost_get_original_poster($type, $entity_id) {
  $uid = db_select('forum_cloakpost', 'f')
    ->fields('f', array('uid'))
    ->condition('entity_id', $entity_id)
    ->condition('type', $type)
    ->execute()
    ->fetchField();
  return $uid;
}


/**
 * Remove identifying author information from an entity.
 */
function _forum_cloakpost_make_entity_anonymous($type, $entity_id, $uid) {
  // Save the original poster information
  $record = new stdClass;
  $record->type = $type;
  $record->entity_id = $entity_id;
  $record->uid = $uid;
  drupal_write_record('forum_cloakpost', $record);

  // Set the uid to 0 for the anonymous user.
  $fields = array(
    'uid' => 0,
  );

  // Handle any specific entity behaviour
  $entity_key = 'entity_id';
  switch ($type) {
    case 'node':
      $entity_key = 'nid';
      break;

    case 'comment':
      $entity_key = 'cid';
      $fields['name'] = variable_get('anonymous', t('Anonymous'));
      break;
  }

  return db_update($type)
    ->fields($fields)
    ->condition($entity_key, $entity_id)
    ->execute();
}


/**
 * Add original poster markup.
 */
function _forum_cloakpost_add_original_poster_markup(&$build, $uid) {
  $account = user_load($uid);
  $build['original_poster'] = array(
    '#prefix' => '<div class="original-poster">',
    '#suffix' => '</div>',
    '#markup' => t("Originally posted by: !username",
      array('!username' => theme('username', array('account' => $account)))),
    '#weight' => 60,
  );
}
