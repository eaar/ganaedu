<style>
	.valor{
		font-size: 18px;
    position: absolute;
    margin-left: 1%;
    color: gray;
	}
	#promedio{
		padding:3%;
	}
	#promedioValue{
		padding:2%;
		border-radius: 8px;
		-webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		border: 1px solid#dddddd;
	}
</style>

<script>
	function calificar(){
		var error = 0;
		var mensaje = "";
		jQuery("#calificar").find(':input').each(function(){
			if(jQuery(this).hasClass("changeValor")){
				if(jQuery(this).val() == 0){
					error = 1;
					mensaje = "Faltan preguntas por calificar.";
				}
			}
		});
		if(error){
			alert(mensaje);
		}else{
			var confirmar = confirm("¿Esta seguro de enviar la encuesta?");
			if(confirmar == true)
				jQuery("#calificar").submit();			
		}
	}
</script>

<div class="cal-escu">
	<form id="calificar" action="#" method="post">
		<!--<label>Seleccionar encuesta</label>
   <select id="filtEncu" name="filtEncu" onchange="submit();">
			<option value="">- Seleccionar -</option>-->
			<?php
			/*
				if($encuestas){
					foreach($encuestas as $encuesta){
						if($encuesta->id == $encuestaSele)
							$vali = "selected";
						else
							$vali = "";
						echo "<option value='".$encuesta->id."' $vali>".$encuesta->name."</option>";
					}
				}
				*/
			?>
		<!--</select>
		<br/><br/>-->
		<span class="titleEscu"><?=$titleEscu?></span>
		<label>Seleccionar escuela</label>
		<select id="filtSchool" name="filtSchool" onchange="submit();">
			<!--<option value="">- Seleccionar -</option>-->
			<?php
				if($schools){
					foreach($schools as $school){
						if($school->code == $schoolSele)
							$vali = "selected";
						else
							$vali = "";

					//	Consultar nombre de la escuela seleccionada
					$schoolCons = db_query("SELECT title_field_value FROM field_data_title_field where entity_id in (select entity_id from field_revision_field_codigo_dane where field_codigo_dane_value = '".$school->code."' )");
					foreach($schoolCons as $schoolCon){
						// $titleSchool = $schoolCon->title_field_value;
					}

						//echo "<option value='".$school->code."' $vali>".$titleSchool."</option>";
					}
				}
			?>
		</select>
		<br/><br/>
		<button class="backCali" type="button" onclick="window.location='/content/califica-tu-escuela';">Volver</button>
		
		<?php
		
			//	Mostrar mensaje
			echo "<h2>".$encuesta->name."</h2>";
			echo "<h4>".$encuesta->description."</h4><br/>";
			echo "<h5 class='titulo_escuela_'>".$titleSchool."<h5>";
			if($mensaje){
				echo "
					<h3 class='mensaje_e'>$mensaje</h3>
				";
			}
		
			if($encuestaSele && $schoolSele && !$mensaje){
				echo "<input type='hidden' id='user' name='user' value='".$user->uid."' />";
				$promedio = 0;
				foreach($preguntas as $pregunta){
					echo "
						<div>
							<p><b>".$pregunta->name."</b></br>".$pregunta->description."</p>
					";
					
					//	Validar si la pregunta contiene imagen
					if(file_exists($_SERVER['DOCUMENT_ROOT']."/sites/default/files/encuestas/".$pregunta->id.".png")){
						echo "<p class='seccionImage'><img class='imagePreg' src='/sites/default/files/encuestas/".$pregunta->id.".png' width='400px' /></p>";
					}
					
					echo "
							<p>".$pregunta->opciones."</p>
							<p><input type='range' value='0' class='changeValor' id='valorSele".$pregunta->id."' name='valorSele".$pregunta->id."' min='0' max='10'>&nbsp;<span class='valor' id='valor".$pregunta->id."'>0</span></p>
						</div>
					";
				}
				echo "
					<h3 id='promedio'>Promedio a calificar: <span id='promedioValue'>0</span></h3>
					<button type='button' onclick='calificar();'>Calificar</button>
				";
			}
		?>
		
	</form>
</div>

<script>
	jQuery(document).ready(function(){
		jQuery(".changeValor").change(function(){
			var id = jQuery(this).attr("id");
			id = id.replace("valorSele","");
			jQuery("#valor" + id).html(jQuery(this).val());
			
			//	Hallar promedio
			var promedio = 0;
			var cont = 0;
			jQuery(".changeValor").each(function(){
				cont = cont + 1;
				promedio = promedio + parseInt(jQuery(this).val());
			});
			promedio = promedio / cont;
			
			<?php
				
				//	Consultar color para asignar
				foreach($configs as $config){
					$bajaDesde = $config->bajaDesde;
					$bajaHasta = $config->bajaHasta;
					$bajaColor = $config->bajaColor;
					$mediaDesde = $config->mediaDesde;
					$mediaHasta = $config->mediaHasta;
					$mediaColor = $config->mediaColor;
					$altaDesde = $config->altaDesde;
					$altaHasta = $config->altaHasta;
					$altaColor = $config->altaColor;
				}
				
			?>
			
			jQuery("#promedio").html("<span id='promedioValue'>Promedio a calificar: " + promedio + '</span>');
			
			if(promedio>=<?=$bajaDesde?> && promedio<=<?=$bajaHasta?>){
				jQuery("#promedioValue").css("background","<?=$bajaColor?>");
			}
				
			
			if(promedio>=<?=$mediaDesde?> && promedio<=<?=$mediaHasta?>){
				jQuery("#promedioValue").css("background","<?=$mediaColor?>");
			}
				
			
			if(promedio>=<?=$altaDesde?> && promedio<=<?=$altaHasta?>){
				jQuery("#promedioValue").css("background","<?=$altaColor?>");
			}
				
			
			
		});
	});
</script>