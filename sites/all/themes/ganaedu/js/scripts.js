(function ($) {
  /**
   * Magic Theme
   */
  Drupal.behaviors.magicTheme = {
    attach: function (context) {
      if($('.ai-primervisita').length > 0){
        $('.primeravisita-close').click(function(){
          $('.ai-primervisita').slideUp();
          Drupal.behaviors.magicTheme.setCookie('primervisita', 1, 30);
        });
        setTimeout(function(){
          $('.ai-primervisita').slideDown();
        }, 10000);
      }
    },
    setCookie: function(name, value, days) {
      var expires = "";
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "") + expires + "; path=/";
    },
    getCookie: function(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
          c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
          return c.substring(nameEQ.length, c.length);
      }
      return null;
    },
    eraseCookie: function(name) {
      document.cookie = name + '=; Max-Age=-99999999;';
    }
  };
	
	jQuery(document).ready(function(){
		
		setTimeout(function(){
			jQuery(".button-limpiar").click(function(){
				alert(1);
			});
		},2000);
		
		setTimeout(function(){
			
			//	Total administrativo
			if(jQuery(".view-id-suma_personal_administrativo").find(".view-footer").html() == undefined){
				jQuery("#block-views-46ff879704280ff6a5a45278eae78f55").hide();
				// jQuery(".view-id-suma_personal_administrativo").html("<p class='conoceEmptyText'>Total Administrativos:</p>");
			}
			
		},3000);
		
		//	Acción en el boton limpiar en el buscador
		jQuery("#edit-reset").attr("type","button");
		jQuery("#edit-reset").click(function(){
			jQuery("#edit-combine").val("");
		});
		jQuery("#edit-combine").keypress(function(){
			setTimeout(function(){
				jQuery("#edit-reset").attr("type","button");
				jQuery("#edit-reset").click(function(){
					jQuery("#edit-combine").val("");
				});
			},3000);
		});
		
		//	Grado 0
		jQuery("#edit-field-grados-und-49").after("0");
		
		setTimeout(function(){
			
			//	Expandir menu al editar una escuela
			var url = window.location;
			url = url.toString();
			url = url.split("?destination=");
			url = url[1];
			switch(url){
				case "admin/dashboard/escuelas":
					jQuery("#ui-accordion-1-header-0").click();
					
					//	Eliminar caracteres especiales en los títulos
					var titlePan = jQuery(".easy-breadcrumb_segment-title").html();
					titlePan = titlePan.replace("&lt;em&gt;","");
					titlePan = titlePan.replace("&lt;/em&gt;","");
					jQuery(".easy-breadcrumb_segment-title").html(titlePan);
					
				break;
				case "admin/dashboard/programas_apoyo":
					jQuery("#ui-accordion-1-header-5").click();
				break;
				case "admin/dashboard/lista-saber-11":
					jQuery("#ui-accordion-1-header-0").click();
				break;
			}
			
			
				
		},1000);
		
		//	Habilitar rol
		jQuery("#edit-field-rol-und").attr("disabled",false);
		
		//	Cambiar palabras ingles a español
		jQuery(".form-item-field-fecha-ingreso-und-0-value-date label").html("Fecha");
		jQuery(".form-item-field-fecha-retiro-und-0-value-date label").html("Fecha");
		jQuery(".form-item-field-fecha-nacimiento-und-0-value-date label").html("Fecha");
		jQuery("#personal-node-form .tabbable").hide();
		
		
		
		
	});
 
})(jQuery);